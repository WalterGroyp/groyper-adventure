

Health
======
* Coziness is the equivalent to health.
* Groyper never dies, he just becomes too dissilusioned to continue his quest.
* Visual meter is a row of tea cups. (Like hearts in zelda games)

Magic
=====
* Frenergy is the equivalent to magic. 
* Frenergy is gained by collecting nostalgia, which is found throughout the world. 
* Frenergy is restored in full when visiting good friends.
* (Not sure yet what the visual representation will be.)

Enemy system
============
* Enemies are not killed, but tamed. There might be exceptions, but this is the general rule.
* Main enemy types are bugs.
* Each bug type comes in one of three classes: common, special, or magic.
* Taming common bugs give exprerience. Might drop tea or nostalgia.
* Taming special bugs teaches Groyper a new attack or mechanic. After taming the first special of a type and learning its mechanic/atttack, special bugs continue to appear throughout the world like common bugs, except more challenging.
* Taming magic bugs teaches Groyper a new bug magic. Magic bugs are like bosses and not encountered again.